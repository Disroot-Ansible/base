# Basic role

This role sets up defaults we use across all newly created containers. It preconfigures various things as well as install few basic packages etc.

example playbook:

```
---

- hosts: all
  roles:
    - base

```

All configurable variables reside in `defaults/main.yml`. If you wish to modify them per host, add them to your `/etc/ansible/host_vars/yourhostname.yml` of for group of hosts in `/etc/ansible/group_vars/yourgroup/main.yml` etc.


## Tags
* `apt`: to update packages
* `hosts`: to deploy only host file
* `ssh`: to deploy ssh
* `shouter`: to deploy shouter


## Note
`go-sendxmpp` is now a package on bookworm.